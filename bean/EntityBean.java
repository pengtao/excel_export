package bean;

import java.io.Serializable;

public class EntityBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String sjhm;
	private String hjlx;
	private String dfhm;
	private String qssj;
	private String thsc;
	private String thdd;
	private String ctlx;
	private String jbhf;
	private String chf;
	private String zhf;

	public EntityBean(String sjhm, String hjlx, String dfhm, String qssj,
			String thsc, String thdd, String ctlx, String jbhf, String chf,
			String zhf) {
		this.sjhm = sjhm;
		this.hjlx = hjlx;
		this.dfhm = dfhm;
		this.qssj = qssj;
		this.thsc = thsc;
		this.thdd = thdd;
		this.ctlx = ctlx;
		this.jbhf = jbhf;
		this.chf = chf;
		this.zhf = zhf;
	}

	public String getChf() {
		return chf;
	}

	public void setChf(String chf) {
		this.chf = chf;
	}

	public String getCtlx() {
		return ctlx;
	}

	public void setCtlx(String ctlx) {
		this.ctlx = ctlx;
	}

	public String getDfhm() {
		return dfhm;
	}

	public void setDfhm(String dfhm) {
		this.dfhm = dfhm;
	}

	public String getHjlx() {
		return hjlx;
	}

	public void setHjlx(String hjlx) {

		this.hjlx = hjlx;
	}

	public String getJbhf() {
		return jbhf;
	}

	public void setJbhf(String jbhf) {
		this.jbhf = jbhf;
	}

	public String getQssj() {
		return qssj;
	}

	public void setQssj(String qssj) {
		this.qssj = qssj;
	}

	public String getSjhm() {
		return sjhm;
	}

	public void setSjhm(String sjhm) {
		this.sjhm = sjhm;
	}

	public String getThdd() {
		return thdd;
	}

	public void setThdd(String thdd) {
		this.thdd = thdd;
	}

	public String getThsc() {
		return thsc;
	}

	public void setThsc(String thsc) {
		this.thsc = thsc;
	}

	public String getZhf() {
		return zhf;
	}

	public void setZhf(String zhf) {
		this.zhf = zhf;
	}

}