package bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SQLBean {
	String url = "jdbc:oracle:thin:@localhost:1521:ORCL";
	Connection con = null;
	Statement sta = null;

	public SQLBean() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection(url, "scott", "tiger");
			sta = con.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ResultSet select(String selects) throws Exception {
		return sta.executeQuery(selects);
	}
}