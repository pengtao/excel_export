package action;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.opensymphony.xwork2.ActionSupport;

/**
 * To change this template use File | Settings | File Templates.
 */
public class OutputExcelAction extends ActionSupport  {
	private static final long serialVersionUID = 1L;
	private String fileName;
	private InputStream excelFile;
	
	public String getFileName() throws UnsupportedEncodingException {
		String tempName = "中文名字"+"-" 
	               + new SimpleDateFormat("yyyyMMddhhmm").format(new Date())
	               + ".xls";
	       //ISO8859-1
	      fileName = new String(tempName.getBytes(), "GBK");
	       System.out.println(fileName);
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public InputStream getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(InputStream excelFile) {
		this.excelFile = excelFile;
	}

	@Override
	public String execute() throws Exception {
		List<String> list = new ArrayList<String>();
		list.add("张三");
		list.add("李四三");
		int rows = 0;
		HSSFWorkbook wk = new HSSFWorkbook();
		HSSFSheet sheet = wk.createSheet("FollowAgentStats");
		HSSFRow row = sheet.createRow(rows++);
		int column = 0;
		HSSFCell cell = row.createCell(column++, HSSFCell.CELL_TYPE_STRING);
		cell.setCellValue("序号");
		cell = row.createCell(column++, HSSFCell.CELL_TYPE_STRING);
		cell.setCellValue("姓名");
		for (String ls : list) {
			row = sheet.createRow(rows++);
			column = 0;
			cell = row.createCell(column++, HSSFCell.CELL_TYPE_NUMERIC);
			cell.setCellValue(rows - 1);
			cell = row.createCell(column++, HSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(ls);
		}
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			wk.write(out);
			excelFile = new ByteArrayInputStream(out.toByteArray());
			return SUCCESS;
		} catch (Throwable th) {
			th.printStackTrace();
			System.out.println("无法输出Excel文件");
			return ERROR;
		}
	}
}
