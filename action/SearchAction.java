package action;

import java.sql.ResultSet;
import net.sf.json.JSONArray;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.opensymphony.xwork2.ActionSupport;

import bean.EntityBean;
import bean.SQLBean;

public class SearchAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Log log = LogFactory.getLog("Poi_mvc");
	private JSONArray dataMap = new JSONArray();
	
	public String execute() throws Exception {
		SQLBean sq = new SQLBean();
		String sql = "select * from detial";
		try {
			ResultSet res = sq.select(sql);
			int i = 0;
			while (res.next()) {
				i++;
				System.out.println(i);
				String sjhm = res.getString("sjhm");
				String hjlx = res.getString("hjlx");
				String dfhm = res.getString("dfhm");
				String qssj = res.getString("qssj");
				String thsc = res.getString("thsc");
				String thdd = res.getString("thdd");
				String ctlx = res.getString("ctlx");
				String jbhf = res.getString("jbhf");
				String chf = res.getString("chf");
				String zhf = res.getString("zhf");
				EntityBean eb = new EntityBean(sjhm, hjlx, dfhm, qssj, thsc, thdd, ctlx, jbhf, chf, zhf);
				dataMap.add(eb);
				log.info("开始封装数据：手机号码=" + sjhm + "呼叫类型" + hjlx + "对方号码" + dfhm + "起始时间" + qssj + "通话时间" + thsc + "通话地点" + thdd + "长途类型" + ctlx + "基本话费" + jbhf + "常话费" + chf + "总话费" + zhf );
			}
			
			System.out.println(dataMap.toString());
			res.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public JSONArray getDataMap(){
		return dataMap;
	}
}